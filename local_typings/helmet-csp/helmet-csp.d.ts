declare module 'helmet-csp' {
	import express = require('express');
	interface IOptions {
		baseUri?: string,
		childSrc?: string [],
		connectSrc?: string [],
		defaultSrc?: string [],
		fontSrc?: string [],
		formAction?: string,
		frameAncestors?: string,
		frameSrc?: string [],
		imgSrc?: string [],
		manifestSrc?: string [],
		mediaSrc?: string [],
		objectSrc?: string [],
		pluginTypes?: string [],
		reportUri?: string,
		sandbox?: string [],
		scriptSrc?: string [],
		styleSrc?: string [],
		upgradeInsecureRequests?: boolean,
		reportOnly?: boolean, 
		setAllHeaders?: boolean, 
		disableAndroid?: boolean, 
		safari5?: boolean
	}
	function helmet(options: IOptions): express.RequestHandler;
	export = helmet;
}