declare module 'zip-stream' {
	import stream = require('stream');
	
	interface IEntry {
		name: string;
		type?: string;
		date?: string|Date;
		store?: boolean;
		comment?: string;
		mode?: number;
	}

	class zipper extends stream.Readable {
		finish(): void;
		entry(input:string|Buffer|stream.Readable, e: IEntry, cb: (err:any, data:any) => void): void;
		entryAsync(input:string|Buffer|stream.Readable, e: IEntry): Promise<any>;
	}
	export = zipper;
}
