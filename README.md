# Bookscan.js

Bookscan.js is a node.js application that controls through gphoto2 a 
pair of cameras to scan a book two pages at a time and create an archive
of images that can be downloaded.

Compared to book-scanner-control, it is accessible with a browser.
It can be deployed on a Raspberry pi giving a fully independent
bookscanner accessible for example from a tablet.

## Installation 

This program has been tested on debian derived distribution (Ubuntu and raspbian). The scripts must be adapted
to run on something else (eg. windows).

The program is written in typescript and you need:
* node
* npm
* bower
* grunt
* typescript compiler (tsc)
* tsd (definitions from definitely typed).

Type 
    npm install; bower install; grunt install

You need gphoto2 and libgphoto2.

## Running

The default port is 3000. You must connect your cameras first. The tool
must be adapted for the camera you are using (I use old Nikon Coolpix 
4100/4600a).

Note for Coolpix 4100 users: you must recompile gphoto2 to change its
capabilities in camlibs/ptp2/interface.c :  
    {"Nikon:Coolpix 4100 (PTP mode)", 0x04b0, 0x012d, PTP_CAP|PTP_NO_CAPTURE_COMPLETE}
