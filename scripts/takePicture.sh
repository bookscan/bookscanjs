#!/bin/bash
port=$1
thumbDir=$2
side=$3

img=$(gphoto2 --port=$port --capture-image | sed 's!^[^/]*\(/[^ ]*\) .*$!\1!')
if [ "$?" != 0 ]; then exit 1; fi
dir=$(dirname "$img")
base=$(basename "$img")

#idx=$(gphoto2 --port=$port -L --folder="${dir}" | grep "${base}" | sed 's!#\([0-9]*\).*!\1!')

if [ "$?" != 0 ]; then exit 1; fi
thumb="TH_${side}${base}"
gphoto2 --port=$port -t="${base}" --folder="$dir" --force-overwrite --filename="${thumbDir}/${thumb}" &> /dev/null
if [ "$?" != 0 ]; then exit 1; fi
echo "{ \"folder\": \"${dir}\", \"name\": \"${base}\", \"thumb\": \"${thumb}\"}"
exit 0
