#!/bin/bash
echo '['
first=1
gphoto2 --auto-detect | grep usb: | while read E; do
  model="${E// (PTP mode)*/}"
  usb="${E//*(PTP mode) /}"
  fc=$(gphoto2 --port ${usb} --get-config /main/capturesettings/focallength)
  min=$(echo ${fc} | sed 's/^.*Bottom: \([0-9]*\) .*/\1/')
  max=$(echo ${fc} | sed 's/^.*Top: \([0-9]*\) .*/\1/')
  current=$(echo ${fc} | sed 's/^.*Current: \([0-9]*\) .*/\1/')
  if [ ${first} == 1 ]; then first=0; else echo ',' ; fi
  echo "{\"model\":\"${model}\", \"usbid\":\"${usb}\", \"min\":${min}, \"focus\":${current}, \"max\":${max}}"
done
echo ']'
