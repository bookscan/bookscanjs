#/bin/bash
port=$1
dir=$2
base=$3
file=$4
#idx=$(gphoto2 --port=$port -L --folder="${dir}" | grep "${base}" | sed 's!#\([0-9]*\).*!\1!') 

#if [ "$?" != 0 ]; then exit 1; fi

gphoto2 "--port=$port" "-p=${base}" "--folder=${dir}" --force-overwrite --filename="${file}" &> /dev/null

exit $?

