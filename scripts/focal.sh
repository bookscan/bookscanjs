#!/bin/bash

port="$1"
focal="$2"
property="$3"

gphoto2 "--port=${port}" --set-config "${property}=${focal}"
exit $?
