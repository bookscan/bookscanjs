#!/bin/bash

gphoto2 --port $1 --storage-info | ( while read L; do
  case "$L" in
  basedir=*)
    basedir=${L#*=}
    ;;
  totalcapacity=*)
    total=${L#*=}
    total=${total% KB}
    ;;
  free=*)
    free=${L#*=}
    free=${free% KB}
    ;;
  freeimages=*)
    images=${L#*=}
    ;;
  esac
done; echo "{ \"basedir\":\"${basedir}\", \"total\":${total},\"free\":${free}, \"images\":${images} }" )
  