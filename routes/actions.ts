/*jslint node:true indent:2 nomen:true es5:true */

/**
 * New node file
 */
import Promise = require('bluebird');
import express = require('express');
import socket_io = require('socket.io');

import camera = require('../model/camera');
import page = require('../model/page');
import upload = require('../model/upload');

var _lock = 0;


export function get_main(req: express.Request, res: express.Response) {
  'use strict';
  if (_lock === 0 && camera.ready()) {
    _lock = 1;
    camera.info()
      .then(function(cams) {
        _lock = 0;
        res.render('index.hbs', { token: req.csrfToken(), camera: cams, scripts: ['control.js'] });
      });
  } else {
    res.render('connect.hbs', { token: req.csrfToken() });
  }
}

export function post_focusLeft(req: express.Request, res: express.Response) {
  'use strict';
  if (_lock === 1) {
    return res.status(500).json({ error: "in use" });
  } else { _lock = 1; }
  var focus = parseInt(req.body.focal, 10);
  camera.focus_left(focus).then(function(output) {
    _lock = 0;
    res.status(200).json(output);
  }).catch(function(err) {
    _lock = 0;
    res.status(500).json({ error: err });
  });
}

export function post_focusRight(req: express.Request, res: express.Response) {
  'use strict';
  if (_lock === 1) {
    return res.status(500).json({});
  } else { _lock = 1; }
  var focus = parseInt(req.body.focal, 10);
  camera.focus_right(focus).then(function(output) {
    _lock = 0;
    res.status(200).json(output);
  }).catch(function(err) {
    _lock = 0;
    res.status(500).json({ error: err });
  });
}

export function post_show(req: express.Request, res: express.Response) {
  'use strict';
  var index = parseInt(req.body.index, 10);
  page.get(index, 2).then(function(pages: page.IPage[]) {
    var result: page.IDoublePage = {};
    pages.forEach(function(p: page.IPage) {
      console.log(p.page + ' ' + index);
      if (p.page === index) {
        result.left = p;
      } else {
        result.right = p;
      }
    });
    return res.status(200).json(result);
  }).catch(function(err: any) {
    res.status(500).json({ error: err });
  });
}

export function post_insert(req: express.Request, res: express.Response) {
  'use strict';
  if (_lock === 1) {
    return res.status(500).json({ error: 'in use' });
  } else { _lock = 1; }
  var index = parseInt(req.body.index, 10);
  console.log(index);
  camera.take_photos_both()
    .then(function(output: page.IDoublePage) {
      if (!output.left) {
        output.left = <page.IPage>{};
      }
      if (!output.right) {
        output.left = <page.IPage>{};
      }
      page.insert(index, [output.left, output.right])
        .then(function() {
          _lock = 0;
          return res.status(200).json(output);
        });
    })
    .catch(function(err) {
      _lock = 0;
      res.status(500).json({ error: err });
    });
}

export function post_upload(io: SocketIO.Server) {
  'use strict';
  return function(req: express.Request, res: express.Response) {
    if (_lock === 1) {
      res.status(500).json({ error: 'in use' });
      return;
    } else { _lock = 1; }
    upload.upload(io).then(function() {
      console.log('fini');
      _lock = 0;
      return res.status(200).json({});
    }).catch(function(err) {
      _lock = 0;
      res.status(500).json({ error: err });
    });
  };
}

export function post_delete(req: express.Request, res: express.Response) {
  'use strict';
  var index = parseInt(req.body.index, 10);
  page.remove(index, 2).then(function() {
    return page.get(index, 2).then(function(pages: page.IPage[]) {
      var result: { left?: page.IPage, right?: page.IPage } = {};
      pages.forEach(function(p: page.IPage) {
        console.log(p.page + ' ' + index);
        if (p.page === index) {
          result.left = p;
        } else {
          result.right = p;
        }
      });
      return res.status(200).json(result);
    });
  }).catch(function(err: any) {
    res.status(500).json({ error: err });
  });
}

export function post_connect(req: express.Request, res: express.Response) {
  'use strict';
  camera.connect();
  res.redirect('/');
}

export function get_count(req: express.Request, res: express.Response) {
  'use strict';
  page.count().then(function(count: number) {
    res.json({ count: count });
  });
}

export function post_clear(io: SocketIO.Server) {
  'use strict';
  return function(req: express.Request, res: express.Response) {
    if (_lock === 1) {
      return res.status(500).json({ error: 'in use' });
    } else { _lock = 1; }
    res.status(200).json({});
    upload.clear(io).then(function() { _lock = 0; });
  };
}

export function post_shutdown(req: express.Request, res: express.Response) {
  res.status(200).json({});
  camera.shutdown().catch(function (err) { console.log(err); }); 
}
