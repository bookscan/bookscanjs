/*jslint node: true indent: 2 */
module.exports = function (grunt) {
  'use strict';
  grunt.initConfig({
    
    ts: {
        
        main: {
	  tsconfig:true
        }
    },
    
    tsd: {
        refresh: {
            options: {
                command: 'reinstall',
                config: 'tsd.json'
            }
        }
    },

    mochaTest: {
      test: {
        src: ['test/specs/**/*.js'],
        options: {
        reporter: 'spec',
        captureFile: 'results.txt'
        }
      }
    },

    copy: {
      install: {
        files: [
          { expand: true, src: ['scripts/**', 'public/**', 'views/**', 'package.json'], dest: 'dist'},
          { expand: true, cwd: 'bower_components/jquery/dist/', src: ['**'], dest: 'dist/public/libs/jquery'},
          { expand: true, cwd: 'bower_components/bootstrap/dist/', src: ['**'], dest: 'dist/public/libs/bootstrap'},
          {expand: true, cwd: 'bower_components/seiyria-bootstrap-slider/dist', src: ['**'], dest: 'dist/public/libs/bootstrap-slider'}
        ]
      }
    }
  });
  
  
  grunt.loadNpmTasks('grunt-mocha-test');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-ts');
  grunt.loadNpmTasks('grunt-tsd');
  grunt.registerTask('install', ['copy', 'tsd', 'ts']);
  grunt.registerTask('default', ['ts']);
  
};
