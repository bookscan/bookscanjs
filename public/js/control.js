/*jslint browser: true indent:2 */
/*global  $,console,io */
console.log('ici');

var index = -2;

function message(msg) {
  'use strict';
  $('#message').text(msg);
}

function csrfToken() {
  'use strict';
  var inputs = $('#csrf :input'),
    values = {};
  inputs.each(function () {
    values[this.name] = $(this).val();
  });
  return values;
}

function slide(side, ev) {
  'use strict';
  var data = csrfToken();
  data.focal = ev.value;
  message('');
  $.ajax('/focus' + side, {
    data: data,
    method: 'POST',
    dataType: 'json',
    success: function (data, status) {
      console.log('success ' + JSON.stringify(data));
      if (data.error) {
        message('Error: ' + data.error);
      } else {
        var file = 'thumbs/' + data.thumb,
          image = $('#imageFocus' + side)[0],
          downloaded = new Image();
        downloaded.onload = function () {
          image.src = this.src;
        };
        downloaded.src = file;
      }
    }
  });
  return false;
}

function slideLeft(ev) {
  'use strict';
  return slide('Left', ev);
}

function slideRight(ev) {
  'use strict';
  return slide('Right', ev);
}

function showIndex() {
  'use strict';
  $('#index').text(index < 0 ? '-' : index);
  $('#indexPlus').text(index < 0 ? '-' : index + 1);
}

function toggle(value) {
  'use strict';
  [ $('#buttonPhoto'), $('#buttonNext'), $('#buttonPrevious') ].forEach(
    function (elt) { elt.prop('disabled', value); }
  );
}

function reset() {
  'use strict';
  var imageLeft = document.images[0],
    imageRight = document.images[1],
    download = new Image();
  download.onload = function () {
    imageLeft.src = this.src;
    imageRight.src = this.src;
  };
  download.src = '/images/gris.png';
}

function doAndShow(url, target, ev) {
  'use strict';
  toggle(true);
  var data = csrfToken();
  data.index = target;
  message('');
  $.ajax(url, {
    data: data,
    method: 'POST',
    dataType: 'json',
    success: function (data, status) {
      if (data.left && data.left.thumb && data.right && data.right.thumb) {
        var thumbLeft = 'thumbs/' + data.left.thumb,
          thumbRight = 'thumbs/' + data.right.thumb,
          imageLeft = document.images[0],
          imageRight = document.images[1],
          downloadLeft = new Image(),
          downloadRight = new Image();
        downloadLeft.onload = function () {
          imageLeft.src = this.src;
        };
        downloadRight.onload = function () {
          imageRight.src = this.src;
          toggle(false);
          index = target;
          showIndex();
        };
        downloadLeft.src = thumbLeft;
        downloadRight.src = thumbRight;
      } else {
        if (data.error) { message(data.error); }
        toggle(false);
      }
    }
  });
  return false;
}

function clickPhoto(ev) {
  'use strict';
  return doAndShow('/insert', index + 2, ev);
}

function clickNext(ev) {
  'use strict';
  return doAndShow('/show', index + 2, ev);
}

function deletePhoto(ev) {
  'use strict';
  if (index >= 0) {
    return doAndShow('/delete', index, ev);
  } else {
    return false;
  }
}

function clickPrevious(ev) {
  'use strict';
  if (index === 0) {
    index = index - 2;
    showIndex();
    reset();
    return false;
  } else if (index < 0) {
    return false;
  } else {
    return doAndShow('show', index - 2, ev);
  }
}


function upload() {
  'use strict';
  $('#buttonDownload').prop('disabled', true);
  message('');
  $.ajax('/upload', {
    datatype: 'json',
    method: 'POST',
    success: function (data, status) {
      $('#buttonDownload').prop('disabled', false);
      if (data.error) { message(data.error); }
    },
    data: csrfToken()
  });
  
  return false;
}

function shutdown() {
  'use strict';
  message('');
  $.ajax('/shutdown', {
    datatype: 'json',
    method: 'POST',
    success: function(data, status) {
	message('shudtown all');
    },
    data: csrfToken()
  });
  return false;
}

function clear() {
  'use strict';
  message('');
  $.ajax('/clear', {
    datatype: 'json',
    method: 'POST',
    success: function (data, status) {
      $('#zipbar').css('width', '0%').attr('aria-valuenow', 0);
      $('#uploadbar').css('width',  '0%').attr('aria-valuenow', 0);
      index = -2;
      toggle(false);
      showIndex();
      reset();
    },
    data: csrfToken()
  });
  return false;
}

function getCount() {
  'use strict';
  $.ajax('/count', {
    datatype: 'json',
    method: 'GET',
    success: function (data, status) {
      $('#zipbar').attr('aria-valuemax', data.count);
      $('#uploadbar').attr('aria-valuemax', data.count);
    }
  });
}


$(document).ready(function () {
  'use strict';
  $('#sliderLeft').slider({tooltip: 'always'}).on('slideStop', slideLeft);
  $('#sliderRight').slider({tooltip: 'always'}).on('slideStop', slideRight);
  $('#buttonPhoto').click(clickPhoto);
  $('#buttonNext').click(clickNext);
  $('#buttonPrevious').click(clickPrevious);
  $('#buttonDelete').click(deletePhoto);
  $('#buttonPrepare').click(upload);
  $('#downloadToggle').click(getCount);
  $('#buttonShutdown').click(shutdown);
  $('#buttonClear').click(clear);
  
  var socket = io();
  
  socket.on('upload', function (msg) {
    var uploadbar, max;
    console.log('upload = ' + msg);
    uploadbar = $('#uploadbar');
    max = parseInt(uploadbar.attr('aria-valuemax').toString(), 10);
    console.log('max = ' + max);
    uploadbar.css('width', (msg * 100 / max) + '%').attr('aria-valuenow', msg);
  });
  
  socket.on('zip', function (msg) {
    var zipbar, max;
    zipbar = $('#zipbar');
    max = parseInt(zipbar.attr('aria-valuemax').toString(), 10);
    zipbar.css('width', (msg * 100 / max) + '%').attr('aria-valuenow', msg);
  });
  socket.on('message', function (msg) {
    message(msg.text);
  });
});
