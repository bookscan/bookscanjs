
import Promise = require('bluebird');
import Packer = require('zip-stream');
import socket_io = require('socket.io');

import path = require('path');
import fs = require('fs');

import camera = require('./camera');
import page = require('./page');
import constants = require('./constants');

var readdirAsync: (dir: string) => Promise<string[]> = Promise.promisify(fs.readdir);
var unlinkAsync: (file: string) => Promise<any> = Promise.promisify(fs.unlink);

Promise.promisifyAll(Packer.prototype);

export function upload(io: SocketIO.Server): Promise<any> {
	var countLoad = 0,
		countZip = 0;
	function upload(cam: camera.ICamera) {
		return function(page: page.IPage) {
			countLoad = countLoad + 1;
			io.emit('upload', countLoad);
			return camera.upload_photo(cam, page);
		};
	}
	return camera.info()
		.then(function(cameras) {
			return Promise.join(
				page.getEven().each(upload(cameras.left)),
				page.getOdd().each(upload(cameras.right)),
				<any>function(r1: any, r2: any) { return 0; }
			)
		}).then(function() {
			return readdirAsync(constants.PAGE_DIR);
		}).then(function(files: string[]) {
			var archive = new Packer(),
				writable = fs.createWriteStream('public/pages.zip');
			writable.on('end', function(ev: any) { io.emit('message', { text: 'Zip ready.' }); });
			archive.on('error', function(err: any) { throw err; });
			archive.pipe(writable);
			return Promise.each(
				files,
				function(file) {
					var stream = fs.createReadStream(path.join(constants.PAGE_DIR, file));
					countZip = countZip + 1;
					io.emit('zip', countZip);
					return archive.entryAsync(stream, { name: file });
				}
			).then(function() { return archive.finish(); });;
		})
}

export function clear(io: SocketIO.Server) {
	return page.remove(0, 99999).then(function() {
		return readdirAsync(constants.PAGE_DIR).each(
			function(file: string) {
				var p = path.join(constants.PAGE_DIR, file);
				return unlinkAsync(p);
			}
		);
    }).then(function() {
		return unlinkAsync('public/pages.zip').catch(function(err: any) { });
    }).then(function() {
		return camera.clear_both();
    }).then(function() {
		io.emit('message', { text: 'Deletion succesful' });
    }).catch(function(err: any) {
		io.emit('message', { text: 'Deletion problem: ' + err });
		console.log(err);
    });
}