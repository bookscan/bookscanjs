/*jslint node:true indent:2 */


/**
 * New node file
 */

import mongoose = require('mongoose');
import Promise = require('bluebird');
import constants = require('./constants');

interface MGAsync {
  execAsync: () => Promise<any[]>,
  saveAsync: () => Promise<any>
}

mongoose.connect(constants.DATABASE);
export interface IPage extends mongoose.Document {
  page: number,
  folder: string,
  name: string,
  thumb: string
}

export interface IDoublePage {
  left?: IPage,
  right?: IPage
}

var pageSchema = new mongoose.Schema({
  page: Number,
  folder: String,
  name: String,
  thumb: String
});

export var Page = <mongoose.Model<IPage>> mongoose.model('page', pageSchema);

Promise.promisifyAll(mongoose);
Promise.promisifyAll(Page);
Promise.promisifyAll(Page.prototype);

function findAndIncrement(filter: {}, k: number, m: string) : Promise <any>{
  'use strict';
  var direction = (k < 0) ? -1 : 1;
  return (<any>Page.find(filter).sort({page: -1})).execAsync().each(
    function (page: IPage) {
      console.log(m + ' ' + JSON.stringify(page));
      page.page = page.page + k;
      return (<any>page).saveAsync();
    }
  );
}

export function remove(i: number, k: number) {
  'use strict';
  var j = i + k;
  return (<any>Page.find({page: {$gte: i}}).remove()).execAsync()
    .then(function () {
      console.log('ICI');
      return findAndIncrement({page: {$gte: j}}, -k, 'R');
    });
}

export function insert(i: number, pages: IPage []) {
  'use strict';
  var k = pages.length;
  console.log(pages.length);
/*  var upd = Page.update({page: {$gte: i}}, {$inc: {page: k}}); */
  return findAndIncrement({page: {$gte: i}}, k, 'I').then(function () {
    return Promise.all(
      pages.map(function (page, idx) {
        page.page =  idx + i;
        var obj = new Page(page);
        return (<any>obj).saveAsync();
      })
    );
  });
}
export function get(i: number, k: number) {
  'use strict';
  var cl = Page.find({page: {$gte: i, $lt: i + k}}).sort({page: 1});
  return (<any>cl).execAsync();
}

export function getEven() {
  'use strict';
  var cl = Page.find({page: {$mod: [2, 0]}}).sort({page: 1});
  return (<any>cl).execAsync();
}

export function getOdd() {
  'use strict';
  var cl = Page.find({page: {$mod: [2, 1]}}).sort({page: 1});
  return (<any>cl).execAsync();
}

export function count() {
  'use strict';
  return (<any>Page.count({})).execAsync();
}
