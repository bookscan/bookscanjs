import child_process = require('child_process');
import path = require('path');
import Promise = require('bluebird');
import page = require('./page');
import constants = require('./constants');

export interface ICamera {
  model: string,
  usbid: string
  side: string,
  focus: number,
  min: number,
  max: number,
  storage: IStorage
}

export interface IStorage {
  total: number,
  basedir: string,
  free: number,
  images: number
}

export interface IScanner {
  left: ICamera,
  right: ICamera,
  other: ICamera []
}


function storage_info(camera: ICamera) : Promise <IStorage> {
  'use strict';
  return <Promise<IStorage>> new Promise(function (resolve, reject) {
    function handler(err: any, stdout: Buffer, stderr: Buffer) {
      if (err) {
        reject(err);
      } else {
        try {
          var out = <IStorage> JSON.parse(stdout.toString());
          console.log(out);
          resolve(out);
        } catch (e) {
          reject(e);
        }
      }
    }
    var command = 'scripts/storage.sh ' + camera.usbid;
    child_process.exec(command, handler);
  });
}

function enumerate() : Promise <IScanner> {
  'use strict';
  return (<Promise <ICamera>> new Promise<ICamera>(function (resolve, reject) {
    function handler(err: any, stdout: Buffer, stderr: Buffer) {
      if (err) {
        reject(err);
      } else {
        try {
          var msg = stdout.toString(), out: ICamera;
          console.log(msg);
          out = JSON.parse(msg);
          resolve(out);
        } catch (e) {
          reject(e);
        }
      }
    }
    var command = 'scripts/list.sh';
    child_process.exec(command, handler);
  })).reduce(function (result: IScanner, camera: ICamera) {
    return storage_info(camera).then(function (storage: IStorage) {
      camera.storage = storage;
      if (camera.model === constants.left.model && camera.storage.total === constants.left.total) {
        camera.side = 'L';
        result.left = camera;
      } else if (camera.model === constants.right.model && camera.storage.total === constants.right.total) {
        camera.side = 'R';
        result.right = camera;
      } else {
        result.other.push(camera);
      }
      return result;
    });
  }, { left: undefined, right: undefined, other: [] });
}


var cameras: Promise <IScanner>;

export function ready() {
  'use strict';
  return cameras !== undefined;
}

export function connect() {
  'use strict';
  cameras = enumerate();
}

function camerasPromise() {
  'use strict';
  if (!cameras) {
    connect();
  }
  return cameras;
}


function take_photo(camera: ICamera) : Promise<page.IPage> {
  'use strict';
  return <Promise<page.IPage>>new Promise(function (resolve, reject) {
    function handler(err: any, stdout: Buffer, stderr: Buffer) {
      if (err) {
        reject(err);
      } else {
        var out = JSON.parse(stdout.toString());
        resolve(out);
      }
    }
    var command =
        'scripts/takePicture.sh ' + camera.usbid + ' ' +
        constants.PUBLIC_DIR + ' ' + camera.side;
    console.log(command);
    child_process.exec(command, handler);
  });
}

function focus(camera: ICamera, focal: number) {
  'use strict';
  return new Promise(function (resolve, reject) {
    function handler_thumb(err: any, stdout: Buffer, stderr: Buffer) {
      if (err) {
        reject(err);
      } else {
        var out = JSON.parse(stdout.toString());
        camera.focus = focal;
        resolve(out);
      }
    }
    function handler_focal(err: any, stdout: Buffer, stderr: Buffer) {
      if (err) {
        reject(err);
      } else {
        var command =
            'scripts/takePicture.sh ' + camera.usbid + ' '  +
            constants.PUBLIC_DIR;
        child_process.exec(command, handler_thumb);
      }
    }
    var legit_focal =
        Math.min(Math.max(focal, camera.min), camera.max),
      command =
        'scripts/focal.sh ' + camera.usbid + ' '  +
        legit_focal + ' ' + constants.FOCAL_PROPERTY;
    child_process.exec(command, handler_focal);
  });
}

export function upload_photo(camera: ICamera, photo: page.IPage) {
  'use strict';
  return new Promise(function (resolve, reject) {
    function handler(err: any, stdout: Buffer, stderr: Buffer) {
      if (err) { reject(err); } else { resolve(null); }
    }
    var file =
        path.join(constants.PAGE_DIR, 'page' + ('0000' +  photo.page).slice(-4)) + '.jpg',
      command =
        '/bin/bash scripts/upload.sh ' + camera.usbid + ' ' +
        photo.folder + ' ' + photo.name + ' ' +  file;
    child_process.exec(command, handler);
  });
}

function clear_camera(camera: ICamera) : Promise<any>{
  'use strict';
  return new Promise(function (resolve, reject) {
    function handler(err: any, stdout: Buffer, stderr: Buffer) {
      console.log('X');
      if (err) { reject(err); } else { resolve(null); }
    }
    var command =
        '/bin/bash scripts/deleteAll.sh ' + camera.usbid + ' ' +
        path.join(camera.storage.basedir, 'DCIM');
    child_process.exec(command, handler);
  });
}

export function clear_both() {
  'use strict';
  return camerasPromise().then(function (cameras) {
    return Promise.join(
      clear_camera(cameras.left),
      clear_camera(cameras.right),
      <any> function (r1: any, r2: any) : any {}
    );
  });
  
}

export function take_photo_left() : Promise<page.IPage> {
  'use strict';
  return camerasPromise().then(function (cameras) {
    return take_photo(cameras.left);
  });
}

export function take_photo_right() : Promise<page.IPage> {
  'use strict';
  return camerasPromise().then(function (cameras) {
    return take_photo(cameras.right);
  });
}

export function focus_left(focal_length: number) : Promise<any>{
  'use strict';
  return camerasPromise().then(function (cameras) {
    return focus(cameras.left, focal_length);
  });
}

export function focus_right(focal_length: number): Promise<any> {
  'use strict';
  return camerasPromise().then(function (cameras) {
    return focus(cameras.right, focal_length);
  });
}

export function take_photos_both(): Promise<page.IDoublePage>{
  'use strict';
  var joiner = function (photo_left: page.IPage, photo_right: page.IPage) {
    return {left: photo_left, right: photo_right};
  };
  return camerasPromise().then(function (cameras) {
    return Promise.join(
      take_photo(cameras.left),
      take_photo(cameras.right),
      <any> joiner
    );
  });
}

export function info() : Promise<IScanner> {
  'use strict';
  return camerasPromise();
}

export function shutdown() : Promise <any> {
  'use strict';
  return new Promise <any> (function (resolve, reject) {
    function handler(err: any, stdout: Buffer, stderr: Buffer) {
      if (err) { reject(err); } else { resolve(undefined); }
    }
    var command = '/bin/bash scripts/shutdown.sh';
    child_process.exec(command, handler);
  });
}

