export var PUBLIC_DIR = 'public/thumbs';

export var PAGE_DIR = 'public/pages';

export var right = { model: "Nikon Coolpix 4600a", total: 1930944};

export var left = { model: "Nikon Coolpix 4100", total: 246912};

export var DATABASE = 'mongodb://localhost/bookscanning';

export var SECRET = 'i_cannot_create_it_for_you';

export var FOCAL_PROPERTY = '/main/capturesettings/focallength';