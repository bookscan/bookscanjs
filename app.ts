import express = require('express');
import Promise = require('bluebird');
import handlebars = require('express-handlebars');
import session = require('express-session');
import mongoose = require('mongoose');
import path = require('path');
import favicon = require('serve-favicon');
import logger = require('morgan');
import cookieParser = require('cookie-parser');
import bodyParser = require('body-parser');
import csurf_func = require('csurf');
import csp = require('helmet-csp');
import frameguard = require('frameguard');
import camera = require('./model/camera');
import actions = require('./routes/actions');
import constants = require('./model/constants');
import http = require('http');
import socket_io = require('socket.io');

var csurf = csurf_func();
var app = express();
var server = (<any>http).Server(app);
var io = socket_io(server);

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.engine('hbs', handlebars({
  extname : 'hbs',
  defaultLayout : 'main.hbs'
}));
app.set('view engine', 'hbs');

app.use(favicon(__dirname + '/public/favicon.ico'));

/* For logging request to the server */
app.use(logger('combined'));

/* Content security policy */

app.use(csp({
  scriptSrc: ["'self'"],
  imgSrc: ["'self'"],
  connectSrc: ["'self'", "ws:"], // for socket.io
  fontSrc: ["'self'"], // required for bootstrap glyphicons.
  objectSrc: [],
  mediaSrc: [],
  frameSrc: [],
  sandbox: ['allow-forms', 'allow-scripts'],
  reportUri: '/report-violation',
  reportOnly: true, 
  setAllHeaders: false,  
  disableAndroid: false,  
  safari5: false 
}));

/* We do not use frames and do not want to be framed */
app.use(frameguard('deny'));

app.use(express.static(path.join(__dirname, 'public')));
app.use(cookieParser());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended : false
}));

app.use(session({
  secret : constants.SECRET,
  saveUninitialized : true,
  resave : true
}));

app.use(function (req: express.Request, res: express.Response, next: Function) {
  'use strict';
  if (req.url === '/report-violation') {
    next();
  } else { csurf(req, res, next); }
});

app.route('/').get(actions.get_main);
app.route('/focusLeft').post(actions.post_focusLeft);
app.route('/focusRight').post(actions.post_focusRight);
app.route('/insert').post(actions.post_insert);
app.route('/show').post(actions.post_show);
app.route('/delete').post(actions.post_delete);
app.route('/connect').post(actions.post_connect);
app.route('/upload').post(actions.post_upload(io));
app.route('/count').get(actions.get_count);
app.route('/clear').post(actions.post_clear(io));
app.route('/shutdown').post(actions.post_shutdown);

server.listen(3000, function () {
  'use strict';
  console.log('listening on *:3000');
});
